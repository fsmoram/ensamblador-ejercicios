;Franklin Stalin MOra
; programa loop presentacion mensaje

%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
section	.data
   saludo db 10,'hola: '
   len_saludo equ $-saludo
section .bss
        iter resb 1
section	.text
   global _start        ;must be declared for using gcc
	
_start:	
    mov ecx, 9 ;para los ciclos

;se necesita etiquetas para loop
for: 
    push rcx
    add rcx,'0'
    mov [iter],rcx
    imprimir saludo,len_saludo
    imprimir iter,1
    pop rcx
    loop for
salir:
    mov eax,1
    int 80h