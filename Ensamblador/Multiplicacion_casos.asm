;Franklin Stalin Mora
;Resta de dos numeros estaticos
section .data
        mensaje_presentacion db "La multiplicacion de los dos numero es: ",10
        len_msg_presentacion equ $-mensaje_presentacion
        new_line db "",10
        len_new equ $-new_line
section .bss
        multiplicacion resb 1
section .text
        global _start

_start:

;************************Caso UNo***********************
    mov al,3
    mov bh,2
;************************************* operacion
    MUL bh
    add al,'0'   ; ax=al*bh
    mov [multiplicacion], al


;*************************Caso 2******************************

;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,mensaje_presentacion		; 
	mov edx,len_msg_presentacion		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,multiplicacion		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux

;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H	