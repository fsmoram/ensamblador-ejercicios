;Franklin Stalin
;loop Cuadrado

%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro
section	.data
	msj db '*'
	new_line db 10,''

section	.text
   global _start        ;must be declared for using gcc
	
_start:	
	
	mov ebx, 9;filas
	mov ecx, 9	;columnas
	jmp principal

principal:
	;filas
	push rbx;
 	cmp ebx, 0    
	jz salir
    jmp for1;

for1:
    push rcx
    imprimir msj
    pop rcx
    loop for1
lineaNew:
   	imprimir new_line ;imprime el salto de linea
	pop rbx;
	dec ebx
	mov ecx, 9
	jmp principal

salir:
	mov eax,1
	int 80h