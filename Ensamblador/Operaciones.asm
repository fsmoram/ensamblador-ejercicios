;Franklin Stalin Mora

;Operaciones estaticas

section .data
        msg_suma db "La suma de los dos numero es: "
        len_msg_suma equ $-msg_suma
        msg_resta db "La Resta de los dos numero es: "
        len_msg_resta equ $-msg_resta
        msg_mult db "La Multiplicacion de los dos numero es: "
        len_msg_mult equ $-msg_mult
        msg_resd db "El residuo de los dos numero es: "
        len_msg_resd equ $-msg_resd
        msg_cos db "El cosiente de los dos numero es: "
        len_msg_cos equ $-msg_cos
        new_line db "",10
        len_new equ $-new_line
section .bss
        suma resb 1
        resta resb 1
        multi resb 1
        ;division
        residuo resb 1
        cosiente resb 1
section .text
        global _start

_start:

;_____________________Suma_________________
    mov eax,4
    mov ebx,2
    add eax,ebx        ;eax=eax+ebx
    add eax,'0'
    mov [suma], eax
   ;________________resta____________________
    mov ax,4
    mov bx,2
    SUB ax,bx
    add ax,'0'
    mov [resta], ax

    ;_________________Multiplicacion_________________
    mov ax,4
    mov bx,2
    MUL bx
    add ax,'0'
    mov [multi], ax

    ;________________Division________________________
    mov al,4
    mov bh,2
    DIV bh
    add al,'0'
    mov [cosiente],al
    add ah,'0'
    mov [residuo], ah

    ;______________________________________Presntacion Suma
;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,msg_suma		; 
	mov edx,len_msg_suma	; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,suma		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux


 ;_________________________Linea
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux


    ;_________________________________Presentacion resta___________________________
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,msg_resta		; 
	mov edx,len_msg_resta	; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,resta		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux


     ;_________________________Linea
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

;______________________________________Presentacion MUltiplicacion________________________

    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,msg_mult		; 
	mov edx,len_msg_mult	; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,multi		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux

     ;_________________________Linea
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

;-___________________________________Presentacion Division______________
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,msg_cos		; 
	mov edx,len_msg_cos	; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,cosiente		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux


     ;_________________________Linea
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux


    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,msg_resd		; 
	mov edx,len_msg_resd	; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,residuo		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux;

    ;_________________________Linea
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H	