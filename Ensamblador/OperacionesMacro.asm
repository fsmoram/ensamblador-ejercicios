;Franklin Stalin Mora
;Operaciones estaticas

%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h

%endmacro

section .data
        msg_suma db "La suma de los dos numero es: "
        len_msg_suma equ $-msg_suma
        msg_resta db "La Resta de los dos numero es: "
        len_msg_resta equ $-msg_resta
        msg_mult db "La Multiplicacion de los dos numero es: "
        len_msg_mult equ $-msg_mult
        msg_resd db "El residuo de los dos numero es: "
        len_msg_resd equ $-msg_resd
        msg_cos db "El cosiente de los dos numero es: "
        len_msg_cos equ $-msg_cos
        new_line db "",10
        len_new equ $-new_line
section .bss
        suma resb 1
        resta resb 1
        multi resb 1
        residuo resb 1
        cosiente resb 1
section .text
        global _start

_start:

;_____________________Suma_________________
    mov eax,4
    mov ebx,2
    add eax,ebx        ;eax=eax+ebx
    add eax,'0'
    mov [suma], eax
   ;________________resta____________________
    mov ax,4
    mov bx,2
    SUB ax,bx
    add ax,'0'
    mov [resta], ax

    ;_________________Multiplicacion_________________
    mov ax,4
    mov bx,2
    MUL bx
    add ax,'0'
    mov [multi], ax

    ;________________Division________________________
    mov al,4
    mov bh,2
    DIV bh
    add al,'0'
    mov [cosiente],al
    add ah,'0'
    mov [residuo], ah

    ;______________________________________Presntacion Suma
;************************************Imprimir mensaje de presentacion********************************************
    imprimir msg_suma, len_msg_suma
    ;************************************imprime valores********************************************
    imprimir suma,1
 ;_________________________Linea
    imprimir new_line, len_new	


    ;_________________________________Presentacion resta___________________________
    ;************************************Imprimir mensaje de presentacion********************************************
    imprimir msg_resta, len_msg_resta
    ;************************************imprime valores********************************************
    imprimir resta,1
 ;_________________________Linea
    imprimir new_line, len_new	
   
;______________________________________Presentacion MUltiplicacion________________________
        ;************************************Imprimir mensaje de presentacion********************************************
    imprimir msg_mult, len_msg_mult
    ;************************************imprime valores********************************************
    imprimir multi,1
 ;_________________________Linea
    imprimir new_line, len_new	

;-___________________________________Presentacion Division______________

;::::::::::::::::::Cosiente
        ;************************************Imprimir mensaje de presentacion********************************************
    imprimir msg_cos, len_msg_cos
    ;************************************imprime valores********************************************
    imprimir cosiente,1
 ;_________________________Linea
    imprimir new_line, len_new	

   ;:::::::::::::::::::::::::::Residuo

     ;************************************Imprimir mensaje de presentacion********************************************
    imprimir msg_resd, len_msg_resd
    ;************************************imprime valores********************************************
    imprimir residuo,1
 ;_________________________Linea
    imprimir new_line, len_new	

    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H	