;Franklin Stalin Mora
;Call tabla de  Multiplicacion
;5 de ago 2020

section .data
    msj1 db 'x'
    msj2 db '='
	new_line db 10,''
 
section .bss
	resultado: resb	2
	num1: resb 2
	num2: resb 2
section .text
 	global _start
 
_start:
	mov ebx, 1
    jmp principal
principal:
    push rbx;
    add rbx,'0'
    mov [num1],rbx 
    mov ecx, 1
	jmp multiplicar

multiplicar:
    push rcx
    mov ax,[num1] ;recoje valor de al
    sub ax,'0'      ; lo conviete en numero
    mul ecx          ; realiza la multiplicacion
    add ax,'0'      ;se transfoma el resultado
    mov [resultado],ax ; se mueve el resultado a resultado
    add cx,'0'      ; se convierte a numero
    mov [num2],rcx
    ;imprimir num1 ; valor 4
    ;imprimir msj1 ; x
    ;imprimir num2 ; incremento de 1 a 9
    ;imprimir msj2 ; =
    ;imprimir resultado  ; resultado
    call imprimir_num1
    call imprimir_msj1
    call imprimir_num2
    call imprimir_msj2
    call imprimir_resul
    call imprimir_new_line
    ;imprimir new_line
    pop rcx
    inc ecx
    cmp ecx,10
    jnz multiplicar
repetir:
    ;imprimir new_line
    call imprimir_new_line
    pop rbx
    inc rbx
 	cmp rbx, 10
	jz salir
	jmp principal
imprimir_num1:
    mov eax,4
    mov ebx,1
    mov ecx,num1
    mov edx,1
    int 80h
    ret
imprimir_num2:
    mov eax,4
    mov ebx,1
    mov ecx,num2
    mov edx,1
    int 80h
    ret
imprimir_msj1:
    mov eax,4
    mov ebx,1
    mov ecx,msj1
    mov edx,1
    int 80h
    ret
imprimir_msj2:
    mov eax,4
    mov ebx,1
    mov ecx,msj2
    mov edx,1
    int 80h
    ret

imprimir_resul:
    mov eax,4
    mov ebx,1
    mov ecx,resultado
    mov edx,1
    int 80h
    ret
imprimir_new_line:
    mov eax,4
    mov ebx,1
    mov ecx,new_line
    mov edx,1
    int 80h
    ret

salir:
	mov eax, 1
	int 80h