;Franklin Stalin Mora
;loop triangulo

%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro
section	.data
	msj db '*'
	new_line db 10,''

section	.text
   global _start        
	
_start:	
	
	mov ecx, 8;filas
	mov ebx, 1	
for1:
    push rcx
    push rbx
    
    ;......nueva linea......
    imprimir new_line
    
    pop rcx
    push rcx
for2:
    push rcx
    imprimir msj
    pop rcx
    loop for2

    pop rbx
    pop rcx
    inc rbx
    loop for1

    
salir:
	mov eax,1
	int 80h