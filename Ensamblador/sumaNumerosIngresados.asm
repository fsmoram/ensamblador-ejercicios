;Franklin Stalin Mora
;Suma de dos numeros estaticos

section .data
        num1 db "Ingrese el numero 1: "
        len_num1 equ $-num1
        num2 db "Ingrese el numero 2: "
        len_num2 equ $-num2
        mensaje_presentacion db "La suma de los dos numero es: ",10
        len_msg_presentacion equ $-mensaje_presentacion
        new_line db "",10
        len_new equ $-new_line
section .bss
        
        numero1 resb 1
        numero2 resb 1
        suma resb 1
section .text
        global _start

_start:
;************************************Imprimir el primer menssaje para el numero 1********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,num1		; 
	mov edx,len_num1		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************lectura del primer menssaje 1********************************************
    mov eax,3		; tipo de sub-rutina, para lecctura
	mov ebx,2		; tipo de estandar  de entrada
	mov ecx,numero1		; Lo almacena en numero, lo que captura en el teclado
	mov edx,2           ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux


;************************************Imprimir el primer menssaje para el numero 2********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,num2		; 
	mov edx,len_num2		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************lectura del primer menssaje 1********************************************
    mov eax,3		; tipo de sub-rutina, para lecctura
	mov ebx,2		; tipo de estandar  de entrada
	mov ecx,numero2		; Lo almacena en numero, lo que captura en el teclado
	mov edx,2           ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux


;**************************************setraduce los valores despues de cada lectura****************
;valores covertidos en digitos
    mov ax,[numero1]
    mov bx,[numero2]
    sub ax,'0'
    sub bx,'0'
;************************************* operacion
    add ax,bx
    add ax,'0'
    mov [suma], ax

;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,mensaje_presentacion		; 
	mov edx,len_msg_presentacion		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,suma		; Lo almacena en numero, lo que captura en el teclado
	mov edx,1          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux

;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,new_line		; 
	mov edx,len_new		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux


    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H	