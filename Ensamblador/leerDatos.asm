Franklin Stalin Mora 

section .data
        mensaje db "Ingrese un numero",10
        len_mensaje equ $-mensaje
        mensaje_presentacion db "El numero ingresado es",10
        len_msg_presentacion equ $-mensaje_presentacion
section .bss
        numero resb 5
section .text
        global _start

_start:
;************************************Imprimir el primer menssaje 1********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,mensaje		; 
	mov edx,len_mensaje		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************lectura del primer menssaje 1********************************************
    mov eax,3		; tipo de sub-rutina, para lecctura
	mov ebx,2		; tipo de estandar  de entrada
	mov ecx,numero		; Lo almacena en numero, lo que captura en el teclado
	mov edx,5           ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux
;************************************Imprimir mensaje de presentacion********************************************
    mov eax,4		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,1		; tipo de estandar
	mov ecx,mensaje_presentacion		; 
	mov edx,len_msg_presentacion		; tamaño del mensaje
	int 80H	   		; interrupción de software para el sistema operativo linux

    ;************************************imprime numero********************************************
    mov eax,4		; tipo de sub-rutina, para lecctura
	mov ebx,1		; tipo de estandar  de entrada
	mov ecx,numero		; Lo almacena en numero, lo que captura en el teclado
	mov edx,5          ;es el numero de reserva, nnumero de caracteres
	int 80H	   		; interrupción de software para el sistema operativo linux


    mov eax, 1						 ; salida del programa, system exit, sys_exit
	int 80H							; interrupción de software para el sistema operativo linux