;Franklin Stalin Mora
;Tabla de Multiplicacion

%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro


section .data
    mensaje_presentacion db "Tabla del "
    msj1 db 'x'
    msj2 db '='
	new_line db 10,''
 
section .bss
	resultado: resb	2
	num1: resb 2
	num2: resb 2
section .text
 	global _start
 
_start:
	mov ebx, 2
    jmp principal
principal:
    push rbx;
    add rbx,'0'
    mov [num1],rbx 
    mov ecx, 1
	jmp multiplicar

multiplicar:
    push rcx
    mov ax,[num1] ;recoje valor de al
    sub ax,'0'      ; lo conviete en numero
    mul ecx          ; realiza la multiplicacion
    add ax,'0'      ;se transfoma el resultado
    mov [resultado],ax ; se mueve el resultado a resultado
    add cx,'0'      ; se convierte a numero
    mov [num2],rcx
    imprimir num1 ; valor 4
    imprimir msj1 ; x
    imprimir num2 ; incremento de 1 a 9
    imprimir msj2 ; =
    imprimir resultado  ; resultado
    imprimir new_line
    pop rcx
    inc ecx
    cmp ecx,10
    jnz multiplicar
repetir:
    imprimir new_line
    pop rbx
    inc rbx
 	cmp rbx, 10 
	jz salir
   
	jmp principal

salir:
	mov eax, 1
	int 80h