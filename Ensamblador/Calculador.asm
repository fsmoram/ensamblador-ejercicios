;Franklin Stalin Mora Mendoza
;13 de julio 2020
;Calculadora basica 

%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h

%endmacro
%macro leer 2
    mov eax,3		
	mov ebx,0		
	mov ecx,%1	
	mov edx,%2    
	int 80H

%endmacro
section .data
	msg1		db		10,'-Calculadora-',10,0
	lmsg1		equ		$ - msg1
 
	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		'Numero 2: ',0
	lmsg3		equ		$ - msg3

	msg4		db		10,'1. Suma ',10,0
	lmsg4		equ		$ - msg4

	msg5		db		'2. Resta ',10,0
	lmsg5		equ		$ - msg5

	msg6		db		'3. Multiplicacion: ',10,0
	lmsg6		equ		$ - msg6

	msg7		db		'4. Division: ',10,0
	lmsg7		equ		$ - msg7

	msg12		db		'5. Ingresar nuevos numeros: ',10,0
	lmsg12		equ		$ - msg12

	msg11		db		'6. Salir: ',10,0
	lmsg11		equ		$ - msg11

	msg8		db		10,'Seleccione la opcion: ',0
	lmsg8		equ		$ - msg8

	msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9
 
	msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10

 
	nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea
 
section .bss
  	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
 ;para la opcon que se escoja
	opcion :    resb    2
section .text
 	global _start
 
_start:


	jmp ingresar
 

ingresar:
; Imprimimos en pantalla el mensaje 1 encabezado
	imprimir msg1,lmsg1

	; Imprimimos en pantalla el mensaje 2 ingrese el numero1
	imprimir msg2,lmsg2

	; Obtenemos el numero 1
	leer num1,2
 
	; Imprimimos en pantalla el mensaje 3 ingrese el numero 2
	imprimir msg3,lmsg3
 
	; Obtenemos el numero 2
	leer num2,2

	jmp	presentar
presentar:
;precentamos el menu 
	imprimir msg4,lmsg4; suma
	imprimir msg5,lmsg5; resta
	imprimir msg6,lmsg6; multiplicacion
	imprimir msg7,lmsg7; division
	imprimir msg12,lmsg12; mensaje opcion
	imprimir msg11,lmsg11; mensaje opcion
	imprimir msg8,lmsg8; opcion

	;leer opcion
	leer opcion,2

	;convertir el valor
	mov ah,[opcion]
	sub ah,'0'

	;comparar los valors ingresado para saber que operacion se realiza
	;JE=jmp (condicional ) if existe una igualdad, salta cuando los valors de los dos operano son giuales zf=1;cf=0

	cmp ah,1
	je sumar

	cmp ah,2
	je restar

	cmp ah,3
	je multiplicar

	cmp ah,4
	je dividir

	cmp ah,5
	je ingresar

	cmp ah,6
	je salir


	; Si el valor ingresado no cumple con ninguna de las condiciones anteriores entonces mostramos un mensaje de error y finalizamos
	; la ejecucion del programa
	imprimir msg10,lmsg10
	jmp salir

 sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9,lmsg9

	; Imprimimos en pantalla el resultado
	imprimir resultado,2

 	jmp	presentar
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9,lmsg9

	; Imprimimos en pantalla el resultado
	imprimir resultado,2

	jmp	presentar
multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9,lmsg9

	; Imprimimos en pantalla el resultado
	imprimir resultado,2

 	jmp	presentar
dividir:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9,lmsg9

	; Imprimimos en pantalla el resultado
	imprimir resultado,2

	jmp	presentar
salir:
	; Imprimimos en pantalla dos nuevas lineas
	imprimir nlinea,lnlinea

	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h
