%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
section .data
    msj1 db "Leer archivos",10
    len1 equ $-msj1
    archivo db "/home/morabtte/Documentos/emsamblador/archivo.txt"
    len2 equ $-archivo

section .bss
    texto resb 30
    idarchivo resd 1
section .text
   global _start        
_start:	    
    jmp principal
principal:
    mov eax,5 ; serviico para crear archivos 
    mov ebx, archivo    ; direcciond el archivo
    mov ecx,0           ; modo de aceso  O-Rdonly=0 -> El archivo se abre solo para leer
                        ; modo de aceso  O-Rdonly=1 -> El archivo se abre para escritura
                        ; modo de aceso  O-Rdwr=2 -> El archivo se abre para escritura y lectura
                        ; modo de aceso  O-Create=256 -> Crea El archivo en caso no exista
                        ; modo de aceso  O-Append=2000h -> Agregar elelmentos en el archivo al final
    mov edx, 777h
    int 80h 

    test eax,eax
    jz salir        ; se ejecut cuando eiste errpres en el archivo
    mov dword [idarchivo],eax
    imprimir msj1, len1

    call leer

    ;____________________cerrar el archivo_______________
    mov eax, 6
	mov ebx, [idarchivo]
	mov ecx, 0
	mov edx, 0
	int 80h    

leer:
    mov eax,3		; tipo de sub-rutina, operación de escritura=>salida
	mov ebx,[idarchivo]		; tipo de estandar
	mov ecx,texto		; 
	mov edx,15		; tamaño del mensaje
	int 80H	 
    ret 
   
salir:
	mov eax, 1
	int 80h