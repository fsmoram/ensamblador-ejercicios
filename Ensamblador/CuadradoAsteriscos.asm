;Franklin Stalin Mora
; programa que imprime asteriscos
%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro
section	.data
	msj db '*'
	new_line db 10,''

section	.text
   global _start        ;must be declared for using gcc
	
_start:	
	
	mov ebx, 9;filas
	mov ecx, 9	;columnas
	jmp principal

principal:
	;filas
	push rbx;
 	cmp ebx, 0    
	jz salir
    jmp asterisco


asterisco:

	dec ecx
	push rcx
	imprimir msj	; ecx se remplza con el '*'
	pop rcx
	cmp ecx,0
	jg asterisco		; jg verifica que el primer operando sea mayor que el segundo operando
	jmp imprimirLinea
imprimirLinea:
   	imprimir new_line ;imprime el salto de linea
	pop rbx;
	dec ebx
	mov ecx, 9
	jmp principal

salir:
	mov eax,1
	int 80h