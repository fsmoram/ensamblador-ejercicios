;Franklin Stalin Mora
;Tabla de Multiplicacion

%macro imprimir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro


section .data
    msjPresenta db 'Tabla de multiplicar de '
    msj1 db 'x'
    msj2 db '='
	new_line db 10,''
 
section .bss
	resultado:	resb 	2
	num1:	resb 	2
	num2:	resb 	2
section .text
 	global _start
 
_start:
	mov al, 2
    add al,'0'
    mov [num1],al
    mov cx,1
    
multiplicar:
    push cx
    mov ax,[num1] ;recoje valor de al
    sub ax,'0'      ; lo conviete en numero
    mul cx          ; realiza la multiplicacion
    add ax,'00'      ;se transfoma el resultado
    mov [resultado],ax ; se mueve el resultado a resultado
    add cx,'0'      ; se convierte a numero
    mov [num2],cx
    imprimir num1 ; valor 4
    imprimir msj1 ; x
    imprimir num2 ; incremento de 1 a 9
    imprimir msj2 ; =
    imprimir resultado  ; resultado
    imprimir new_line
    pop cx
    inc cx
    cmp cx,10
    jnz multiplicar
   

salir:
	mov eax, 1
	int 80h